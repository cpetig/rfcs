 Feature Name: `fermentation`
- Start Date: 2021-06-20
- RFC Number: 
- Tracking Issue:

# Summary
[summary]: #summary

The point is to separate consumables `food` from `potions` and make element of preparation more important. 
Potions ferment with time. Strong potions have less time to become fermented ones. And this fermented potion can have other stats, not only negative ones and of course not the same as they were before.
- Maybe it generates random effects. Not sure about it, think the next version is better.
- Maybe it generates a specific effect. It allows a player to produce some potions by converting them with time in your pocket + it gives a feeling that your potion can be useful even after that process.
The point of this process is to motivate players to use consumables. So, you need to use it, not keep it for a long time, otherwise, it becomes different.

# Motivation
[motivation]: #motivation

1) It separates `potions` from other consumables. 
2) It motivates player to prepare for expedition. In many games you can store potions from the beginning till the end and never use them.
3) Adding `fridge` or `basement` to player's house to null or speed up fermentation process can give more value in having your own building.
4) It allows player to use time as one of crafting materials. 
5) It allows us to see in which direction we can go with non-combat skill tree.

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

Player wants to heal. They know they have bread and two healing potions: one is strong and other one is weak. They open inventory and see `rotten bread`, `strong healing potion` and `potion of strength`. Bread became useless, strong one stays, weak one became even more profitable because of fermentation. Player had no meaning to use it for so long, maybe now it can be good in pair with strong healing potion as other effect.
Why potions ferment? The greatest thing about fantasy worlds is that they can have their own rules. We don’t know if healing potions could ferment, rot or be stronger with years in real life ‘cause we don’t have healing potions. 

# Drawbacks
[drawbacks]: #drawbacks

## Performance
Don't think that it can be heavy for PC. It dooesn't have to be animated or something - just replacing `weak potion` with other `potion`.

## Complexity
Some players can find it annoying. You make lots of weak and cheap healing potions for nothing, because everything ferments. Or otherwise, they don't want to spend time for fermentation to recieve `potion of strength`. But we can have things such as `fridge`, `basement` and skills which allow player to control this process.

# Rationale and alternatives
[alternatives]: #alternatives

## Rationale

The hardest part of this is to make products of fermentation fair. Maybe `potion of speed` would be better to gain from `healing potion`, who knows.

## Alternatives

### classic
To make `potions` eternal compare to `food`. Imho it is too boring and common.

# Prior art
[prior-art]: #prior-art

I can't remember any system like this actually. But in Don't Starve, for example, we can see food rotting process till it becomes "rot". My system is much more complex and interesting.

# Unresolved questions
[unresolved]: #unresolved-questions

## What parts of the design do you expect to resolve through the RFC process before this gets merged?
Overall feedback.

## What parts of the design do you expect to resolve through the implementation of this feature before stabilization?
Balance obviously. This system have lots of potential. But the hardest part is balance.

## What related issues do you consider out of scope for this RFC that could be addressed in the future independently of the solution that comes out of this RFC?
I expect the details of interactions with other systems (especially with skills) to co-evolve with this feature, possibly with additional RFCs.
