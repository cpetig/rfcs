
- Feature Name: steel_worms_buffet
- Start Date: 2021-04-18
- RFC Number: 
- Tracking Issue: 

# Summary

A new dungeon type, using spaghetti cave generation. Several variatiants, several new enemies, items, features, debuffs, behaviours and lore.

# Motivation

Lack of dungeon types, 

# Guide-level explanation

## Steel Worms Buffet
New dungeon type with several varations to keep it interesting and fresh!

### [Hard] Variation 1 (Active Buffet)

#### Enemies

Steel Worm Babies: They attack by going into the wall, and suddenly bursting out from a random direction at you. If you can dodge the attack, they will be stunned for a little bit so you can damage them, and then they'll dig back into the walls to attack again. They are pretty tough and do fairly high amounts of damage (since their attack is dodged fairly easily). You can also hear a vibrating sound as they approach so you will know when they attack. Be careful tho, because when they regain consciousness they will flail around and deal some damage, so you'll have to be careful with timing

Steel Worms: Similar attack, but does not get stunned. Much longer body tho, so you have time to attack it while it turns around for another attack. But once again, be careful, as they have spike-holes which will shoot at you.

(Rare Enemy) Cobalt Worm: Shimmering blue color, same as the Steel Worm but much tougher, and a bit faster. It's tail's orb is green. (Every other enemy's tail-orb is yellow)

(Boss) Steel Worm Queen: It's attack is similar, however, it does not get stunned if you dodge it. Because their body is long, you'll still be able to get a couple of hits in tho. It also has another attack which makes giant rocks fall from the ceiling by making the whole cave rumble.


#### Drops

Steel Worm Babies: a bit of rocks and maybe some low-tier ores/metals

Steel Worms: same as previous but rare drop: "Steel Worms Orb" can be held in hand. If low-tier ore is nearby, it vibrates depending on how close. Can be crafted into "Necklace of Vibration", which has the same effect but you don't have to hold it. (both glow)

Cobalt Worms: a bit of rare materials, and very rare drop: "Cobalt Worms Orb", works the same as the previous but can detect more rare ore. Can also be crafted into a necklace ("Necklace of Greater Vibration")

Steel Worm Queens: maybe slightly rare ore, but very rare drop: "Queen Worm's Wedding Crystal"



### [Medium-Hard] Variation 2 (Finished Buffet, "Troll Age")

#### Enemies

Stone Troll: big monster, large detection range unless you are crouching, then it's just normal detection range. When killed, leaves a pool of acid for a while which you should be really wary of, as standing in it will kill you swiftly.

Dwarves: short but strong humanoids, attacks with pickaxe/axe.

Boss? Dunno right now.


#### Drops

ST: stones, and rare drop: "Stone Troll's Hooves" can be used to make special "shoes" for your mounts to make them faster. Only works on ground-type mounts.

Dwarves: maybe some mining equipment, and an already existing item, Dwarven Cheese.



### [Easy-Medium] Variation 3 (Nature's Overtake)

#### Enemies

Just regular cave enemies

#### Drops

I guess regular cave drops



### [non] Variation 4 (Filled, "Back to Whole")

Just looks like a hole that was filled in with rocks and dirt.



### [Super Hard] Variation 5 (Dwarves Halls)

#### Enemies

Dwarf Soldiers: Dwarves that are prepared to fight, unlike the Dwarf Miners from Variation 2

### [Ultra Hard] Variation 6 (Queen's Grave)

#### Special

"Immoral" debuff is active while in the dungeon, can't be cured, lasts for a while even after leaving the dungeon. Reduces your damage, may also cause "Trauma" which can only be removed by a specialized doctor/medic/healer/mage/wizard/etc.

#### Enemies

Weeping Steel Worm Baby: Similar to the Steel Worm Babies, but the stunned time is much shorter. Slightly blue in appearance. While fighting, "Pity" debuff is activated, damage is reduced.

Mourning Steel Worm: Similar to the Steel Worms, but slightly red, much stronger, and a bit faster. When the worm hits you it also grants "Violently Bleeding" debuff.

(Rare Enemy) Cobalt Worm of Sorrow: same as the other, but an even deeper blue color. It will also make the ground vibrate, lowering your speed by 20%.

(Boss) Steel Worm King: At first, the Steel Worm King will be gently tied around the petrified body of the Steel Worm Queen, waiting for their last moments. To activate the fight, the King has to be lowered to 90% health, after which it will enter it's first phase, "Enraged". It will attack similar to how the Queen did, but it's attacks will constantly make rocks fall from the ceiling, rather than it being a separate attack. It also shoots out metal rods, which if hit you, will stay in you for a long while. Each metal rod in your body will act as -10% to your armor, capping at 50%. It will also cause you to bleed, which can't be cured.

#### Drops

WIP

### Lore and information

So let me explain the variations, and why they exist. So Variation 1 is the first stage, where the Queen already dug out the main hole, and the Birthing Hole. The Queen then does the birthing and then there will Steel Worms and Steel Worm Babies, and of course rarely a Cobalt Worm. Usually Queen's only dig a couple of Buffet's before coming to an eternal rest. At that point, their Wedding Crystal will break and disappear, while the Queen turns into stone and ores.

Speaking of the Wedding Crystal, the way these creatures life cycle works is pretty interesting from what I observed. First of all, when they are born they have no gender. After a couple decades they reach adulthood, in which they will already be divided into males and females, depending on what kind of ores and crystals they consumed. After another couple decades, males will try to find large rare crystals to give to the females, after which a Queen is born. Most males are unsuccessful in finding said large rare crystals, so that's why the world wasn't devoured by insane amounts of Steel Worm Queens. After a quick "honeymoon", the Queen goes onto doing what I wrote before.

Now, once they are done digging around and stuff, which could take anywhere from a week to even a decade, they will move onto another hole, however, Baby Steel Worm's will devour any flowers nearby before leaving with the Queen. **Why is that important?** You can tell what tier the Buffet will be just by the hole's appearance. If there are flowers and rocks, then it is still active and is filled with Steel Worms.
If there are no flowers but there are rocks, then Stone Trolls took over the hole to find any left overs, and brave Dwarves also scout the hole.
If there are no flowers and no rocks, that means the Stone Trolls and Dwarves have also left already, so it just became a regular hole.


## Random Note

I honestly believe that variation is one of the most overlooked aspects of a game.
It adds so much immersion, replayability, and pretty much everything, that when done right,
it'll turn anything into remarkable. Combine that with a good game (Veloren) and you have
a recipe for legendary.


# Reference-level explanation

The dungeon requires a couple of new engine features: segmented enemies for the worm hitboxes, and voxel fluid dynamics for the acid pools dropped by stone trolls.

## Segmented enemies

Excerpted discord discussions (TODO: summarize and combine features)

```
aweinstock [GMT-4]
we could handle segmented enemies the same way terraria does (a chain of entities with a follows-relation to the next one); segmented enemies take more damage from fireballs/piercing weapons that way, but that's kind of a feature
Gemu [GMT +2]
Depends
my kind of wyvern is the classic winged one :eyes:
Snowram [GMT+2]
can't wait for wyverns to split in half when server is getting laggy
Gemu [GMT +2]
wouldnt it just be an advanced snake skeleton
aweinstock [GMT-4]
if we have the segment-relation system and component in common, the client can update the positions of the segments; the bigger issue is with orientation interpolation causing them to potentially spin at high speeds in the presence of lag
```

```
Desttinghim [GMT-6]
When are trains coming to veleron?
zesterer [GMT+0]
Unironically, soon. Airships seem to chain their physics quite well so I'll have to add them in some form soon
aweinstock [GMT-4]
the notion of a mounting-like follows-relation came up in the context of segmented chinese-mythology-style dragons as sky enemies (c.f. terraria's wyverns); it'd also work for making train cars connect to each other, and we already have voxel colliders
```

```
aweinstock [GMT-4]
is anyone working on segments for chinese-style dragons/terraria-style wyverns? would need a head, body segment to be replicated, and a tail
Gemu [GMT +2]
There was a snake skeleton in the works with body segments
aweinstock [GMT-4]
each segment can be multiple animation bones too, potentially (so the entities can curve to follow each other)
Gemu [GMT +2]
We should probably design a skeleton which would fit both snakes, asian dragons and serpents all the like
every skeleton can have up to 16 parts
aweinstock [GMT-4]
I think doing it as a follows-relation with separate entities would work well (have segmented enemies take more AOE damage, just like in terraria), and would also handle kraken tentacles nicely
Sarra_Kitty [GMT-5]
oooo I love that
Great wyvern!
aweinstock [GMT-4]
(the tentacle segments can propagate damage to their owner, and we could have each entity position itself between the kraken and the tentacle tip, and keep the tentacle tip within some radius of the owner, so it can swing them independently)
Gemu [GMT +2]
Sounds good, but im not sure if it will warrant for the bone limit
unless its something that would bypass that as being seperate entities or something
aweinstock [GMT-4]
colliders are still cylinders, we'd need segment colliders for a snake/tentacle to have the right shape for damage
```

## Fluid dynamics

Excerpted discord discussion (TODO: summarize)
```
aweinstock [GMT-4]
if individual player manipulation of fluids can be made to only cost a few KB of RAM per player though, it'd be considered? (in terms of balancing what kind of fluid effects players are allowed access to, I know that things like noita's "trail of water" spell modifierwould probably be a denial of service)
Sharp [GMT +2]
Yeah if it could then it would probably be considered
That'd be an interesting approach
Try to make stuff that's typically calculated on a terrain basis tied to the invoking players instead
If we could figure out a way to make that work I think we could end up with a lot of cool features.
aweinstock [GMT-4]
I'm picturing things like a bucket being emptied resulting in a small pool of water voxels that flows downhill, with the changes contained within an AABB that moves with the flowing water
and that definitely feels like it should only be a few hundred bytes at most per bucket-emptying
Sharp [GMT +2]
Well, if you can figure out a way to do it, great
aweinstock [GMT-4]
(like, a byte of the water level per affected voxel, 24 bytes per AABB, which there should only be one of if the water doesn't get split by an obstacle)

actually; I think this might be doable as-is with terrain colliders: the entire released bucket of water is an entity that maintains the fluid levels of the fluid within it (growing the AABB, and maybe splitting into two entities with smaller AABBs based on heuristics), fluid::Sys operates over entities with with a Fluid component and does cellular automata between them and the terrain, and then updates a Dyna collider based on the fluid levels (e.g. over half full? collide as a water block), and this is scheduled before phys::Sys so that the fluid flow is used for entity-terrain movement in the same frame
Sharp [GMT +2]
I have no idea how well terrain colliders actually scale though
Being able to support them in limited situations is different from something players can trigger pretty arbitrarily.
aweinstock [GMT-4]
they have spatial grid broadphase, so they shouldn't be too bad?
Sharp [GMT +2]
Maybe... I guess we can test and find out
It's kinda crazy we got them working so quickly anyway.
aweinstock [GMT-4]
for fluid dynamics, we also don't need general voxel-voxel collision, since the fluid propagation is CA based separate to the physics (and I'm picturing the fluid entity as an axis-aligned, integer-motioned thing that maybe updates the center and AABB discretely to handle being near the water voxels, but isn't subject to physics by itself)
```

# Drawbacks

# Rationale and alternatives

# Prior art

# Unresolved questions
