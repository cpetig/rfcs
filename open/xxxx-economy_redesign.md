- Feature Name: economy_redesign
- Start Date: 2021-06-20
- RFC Number: 
- Tracking Issue: 

# Summary
[summary]: #summary

Redesign economy simulation for better integration into the game.

# Motivation
[motivation]: #motivation

The recent addition of ores and craftable high end armor to the game made the first economic design (where armor was only obtainable from dungeons) obsolete. We desperately need a better representation of the economic situation.

The most obvious problems are:

- Severe disconnect between item price and stats
- Price imbalance between different Goods due to crafting (e.g. when crafting one Good out of another type)
- Totally starved towns with no trade possibility

I would like to see:

- Towns close to the political border have difficulty to prosper due to frequently destroyed resources by raids, pillaging 
- Border towns will have more military and baggage train (surrounding infrastructure)
- Towns in strategical positions (mountain pass, rivers, lakes, capitals) will prosper
- Different transportation means should have different traits:
  - air ship should prefer light goods and have no dependency on terrain, but perhaps on weather
  - ground transport should be easy to set up but needs constant street maintenance. Sites along the path can offer convenience goods (changing mounts, accomodation) and might prosper. Horses are very fast but low volumen, carts are slow but can transport large amounts.
  - water transport hardly needs infrastructure (besides harbors or jetties/docks) to enable unloading ships without grounding them), it provides huge transport capacity.
- Toll stations? Taxes? Bridge fees?
- Tourism, famous views, religious pilgrimages
- Within an empire different towns will have populations with differing wealth (according to the importance of a town)
- Neighboring dungeons should negatively impact a town's economy (plundering, missing persons)

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

## The following short term proposals:

- Add the following Goods:
  - Ore
  - Coal
  - Gem
  - Housing (needed by everyone)
  - Building ground resource (limits housing)
  - Dungeon Loot (with level?), how to put value to this? (productivity multiplier for Guards and Merchants)
  - Cave Loot (level?) (if not Ore, Coal, Gem?)
  - Animal Loot(Biome)
  - Plants(Biome)
  - Separate ClothArmor, MetalArmor, HideArmor

- Add the following professions:
  - Builder

- Add a cave biome to resources?

- Introduce non-consumed goods to econsim

- Move Good to item equivalence into the item.ron file? This would solve the dependency on path naming and loot table balancing - once and for all.

It feels like Goods need a tier specifier or a biome specification, e.g. you can't grow wool in desert areas, but you can grow camel hair. Sunsilk is desert specific, gold and silver might get in the future. Dragonscale feels totally different from normal leather.

## Long term proposals:

- Introduce a tech level (stone age to renaissance), agriculture got so much more efficient over time, housing changed etc.
- a separate magic level to describe lore within this culture/site

- there should be different life styles accustomed to natural resources (replace burning wood by dung, animal fat; replace vegetables by meat in icy regions; different rules for desert areas; different house materials and styles, wood hut vs tent vs stone house vs cave house vs igloo)

- Placement of towns should adhere to strategic positions

- Do we want mines (artificial) in addition to caves?
- What about exhausting ores in a mine?
- Three/five different armor classes (metal, cloth, hide (stone, wood))?

- What about luxuries, speciality foods etc.?

- Long term profession ideas:
  - teacher, scribe
  - priest
  - musician, actor, poet, singer
  - tax collector
  - executioner
  - entertainer

- faction minted coins

- Long term infrastructure goods (part of inventory?)
  - Mill
  - Houses
  - Schools
  - Tech/Mage Level
  - these should need some goods to build (increase), maintain and should fall in disrepair/vanish if seriously undersupplied

These feel like professions, and people/population/professions could be represented in stock as well - does this make things more simple? This could enable people fleeing from starvation/to better places.

# Unresolved questions
[unresolved]: #unresolved-questions

- We definitely need to figure out how to simulate political factions:

  - how to split an existing empire in half
    - heritage differences
    - insurgences, coups, revolts
  - how to model changing alliances
  - when will neighboring sites pool, when will they fight, when will they trade?
  - Tech/magic levels should be far more levelled within a faction
  - Culture/style/art/breed/wealth should be more uniform within a faction
  - A nation should provide protection to the inner lands but will collect taxes
  - How to identify military advantageous locations

- Sites should be placed according to resources, see https://discordapp.com/channels/449602562165833758/534844039007305729/840680623781707867 for a lenghty discussion on this. Zesterer mentioned sites getting founded/spawn from other sites - this could be an interesting model: Pick some extremely resource rich places on the map and start civilization there.
- Do we have any idea about non-random Dungeon placement (high chaos (another dimension introduced by worldgen, like humidity and temperature) rating?)

Right now econsim is non-quantized, should we change this?

Man years (in specific professions?) could become an econsim Good, this would make item price more predictable.

# TODO

decide about these chapters:

- Reference-level explanation
- Drawbacks
- Rationale and alternatives
- Prior art
